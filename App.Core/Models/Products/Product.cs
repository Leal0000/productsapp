﻿using App.Core.Dependences;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Models.Products
{
    public class Product : BaseEntity, IAgregateRoot
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public Product(string name, string description, string code)
        {
            Name = name;
            Description = description;
            Code = code;
        }
    }
}
