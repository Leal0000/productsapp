﻿using App.Core.Dependences;
using App.Core.Models.Products;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace ASPWerbService.Controllers.Products
{
    public class ListProductsEndpoint : EndpointBaseAsync.WithoutRequest.WithActionResult<ListProductsResponse>
    {
        private readonly IRepository<Product> _repository;
        public ListProductsEndpoint(
             IRepository<Product> repository
        )
        {
            _repository = repository;
        }
        [HttpGet("api/products")]
        [SwaggerOperation(
            Summary = "Gets a list of all products",
            Description = "Gets a list of all products",
            OperationId = "Products.List",
            Tags = new[] { "ProductsEndpoints" })
        ]
        public override async Task<ActionResult<ListProductsResponse>> HandleAsync(
            CancellationToken cancellationToken = default
        )
        {
            var response = new ListProductsResponse();
            var list = await _repository.ListAsync( cancellationToken );
            response.Items = list.Select(e => new ProductDto()
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
                Code = e.Code,
                IsDeleted = e.IsDeleted,
            }).ToList();
            return Ok(response.Items);
        }
    }
}
