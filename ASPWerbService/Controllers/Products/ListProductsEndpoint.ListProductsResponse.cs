﻿using App.Core.Models.Products;

namespace ASPWerbService.Controllers.Products
{
    public class ListProductsResponse
    {
        public List<ProductDto> Items { get; set; }
    }
}
