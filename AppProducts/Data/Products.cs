﻿using App.Core.Models.Products;

namespace AppProducts.Data
{
    public class Products
    {
        public List<ProductDto> products { get; set; } = new List<ProductDto>();
    }
    public class ProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public bool IsDeleted { get; set; } 
    }
}
