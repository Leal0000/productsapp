﻿using Ardalis.Specification;
using Ardalis.Specification.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Dependences
{
    public class EFRepository<T> : RepositoryBase<T>,IAgregateRoot,IRepository<T> where T : class, IAgregateRoot    {
        public  EFRepository(StoreContext dbContext) : base(dbContext)
        {
        }
    }
}
