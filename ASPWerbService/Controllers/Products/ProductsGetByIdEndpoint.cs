﻿using App.Core.Dependences;
using App.Core.Models.Products;
using Ardalis.ApiEndpoints;
using Azure;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace ASPWerbService.Controllers.Products
{
    public class ProductsGetByIdEndpoint : EndpointBaseAsync.WithRequest<Guid>.WithActionResult<ProductDto>
    {
        private readonly IRepository<Product> _repository;
        public ProductsGetByIdEndpoint(
             IRepository<Product> repository
        )
        {
            _repository = repository;
        }
        [HttpGet("api/products/{id:guid}")]
        [SwaggerOperation(
           Summary = "Get a product",
           Description = "Get a product",
           OperationId = "Products.GetById",
           Tags = new[] { "ProductsEndpoints" })
       ]
        public override async Task<ActionResult<ProductDto>> HandleAsync(
           [FromRoute] Guid id,
           CancellationToken cancellationToken = default
       )
       {
            var product = await _repository.GetByIdAsync( id );
            if ( product == null )
            {
                return NotFound();
            }
            var response = new ProductDto()
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Code = product.Code,
                IsDeleted = product.IsDeleted,
            };
            return Ok(response);
       }
    }
}
