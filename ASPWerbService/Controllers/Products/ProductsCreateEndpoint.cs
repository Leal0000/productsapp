﻿using App.Core.Dependences;
using App.Core.Models.Products;
using Ardalis.ApiEndpoints;
using Azure;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace ASPWerbService.Controllers.Products
{
    public class ProductsCreateEndpoint : EndpointBaseAsync.WithRequest<ProductCreateDto>.WithActionResult<ProductDto>
    {
        private readonly IRepository<Product> _repository;
        public ProductsCreateEndpoint(
             IRepository<Product> repository
        )
        {
            _repository = repository;
        }
        [HttpPost("api/products")]
        [SwaggerOperation(
           Summary = "Create a products",
           Description = "Create a products",
           OperationId = "Products.Create",
           Tags = new[] { "ProductsEndpoints" })
       ]
        public override async Task<ActionResult<ProductDto>> HandleAsync(
           [FromBody] ProductCreateDto request,
           CancellationToken cancellationToken = default
       )
        {
            var newItem = new Product(request.Name, request.Description, request.Code);
            newItem = await _repository.AddAsync(newItem);
            var response = new ProductDto()
            {
                Id= newItem.Id,
                Name=newItem.Name,
                Description=newItem.Description,
                Code=newItem.Code,
                IsDeleted=newItem.IsDeleted,
            };
            return Created($"api/products/{newItem.Id}",response);
        }
    }
}
